Number Two

We are going to write a function called findWords that will accept a string, str, as an argument.
The goal of the function is to output the same string but with every word in the string camel-cased.
let allows you to declare variables that are limited to the scope of a block statement.
Forexample:

let count = 1

The for statement starts by declaring the variable i and initializing it to 0. It checks that i is less than the string, performs the two succeeding statements, and increments i by 1 after each pass through the loop. Forexample;

for (let i = 1; i < str.length - 1; i++)

Traverse the string character by character and check whether all the requirements are met for it to be a valid identifier i.e. first character can only be either ‘_’ or an English  and the rest of the characters must neither be a white space or any special character. Forexample;

if (str[i]>= 'A' && str[i]<= 'Z')

The increment operator (++) increments (adds one to) its operand and returns a value forexample count++;

Return Value:  Returns the count of the element in that collection

The console.log() is a function in JavaScript which is used to print any kind of variables defined before in it or to just print any message that needs to be displayed to the user.
Forexample
 console.log( findWords(str));
let str = "kanzuCodeFoundationWordpressBootcamp";
 Out Put = %


 Number One
The numbers are 1, 2, 3, 4, and 5. Then we shall Calculate the following sums using four of the five integers:

Sum everything except 1, the sum is 2 + 3 + 4 + 5 = 14.
Sum everything except 2, the sum is 1 + 3 + 4 + 5 = 13.
Sum everything except 3, the sum is 1 + 2 + 4 + 5 = 12.
Sum everything except 4, the sum is 1 + 2 + 3 + 5 = 11.
Sum everything except 5, the sum is 1 + 2 +3 + 4 = 10.

Add all array elements using reduce.
Subtract from the sum the minimum value in the array. This get the maximum sum is 2 + 3 + 4 + 5 = 14.
Subtract from the sum the maximum value in the array. This get the minimum sum is is 1 + 2 +3 + 4 = 10.

Out Put = 10 14
